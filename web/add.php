<?php
require 'vendor/autoload.php';
$client = Elasticsearch\ClientBuilder::create()
->setHosts(['localhost:9200'])
->build();

if($client){
    echo 'Conexión exitosa </br>';
}
else{
    echo 'Conexión fallida </br>';
    exit;
}

$params = [ 
    'index' => 'casa',
    'type' => 'dueños',
    'id' => '10',
    'body' => ["nombre" => "Eugenio","edad" => 36]
];
$result = $client->index($params);
print_r($result);
var_dump($result);
?>